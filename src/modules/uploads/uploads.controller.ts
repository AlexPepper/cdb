import { Controller, UseInterceptors, UploadedFile, Post } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express'
// import { UploadsService } from './uploads.service'

@Controller('uploads')
export class UploadsController {
  // constructor(private readonly uploadsService: UploadsService) {}
  @Post('/')
  @UseInterceptors(FileInterceptor('file', {
    dest: './src/storage'
  }))
  uploadFile(@UploadedFile() file) {
    console.log(file);
  }
}