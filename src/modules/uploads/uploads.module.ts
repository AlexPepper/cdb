import { Module } from '@nestjs/common';
// import { UploadsController } from './uploads.controller'
// import { FilesController } from './files/files.controller';
// import { ImagesController } from './images/images.controller';
import { ImagesModule } from './images/images.module';
import { FilesModule } from './files/files.module';
import { UploadsService } from './uploads.service';

@Module({
  // controllers: [FilesController, ImagesController],
  imports: [ImagesModule, FilesModule],
  // providers: [UploadsService],
})

export class UploadsModule {}
