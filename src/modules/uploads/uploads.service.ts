import { Injectable } from '@nestjs/common';

@Injectable()
export class UploadsService {
  getHello(): string {
    return 'Hello World!';
  }

  getFileName(req, file, callback) {
    const fileExtName = file.originalname;
    const randomName = Array(4)
      .fill(null)
      .map(() => Math.round(Math.random() * 16).toString(16))
      .join('');
    callback(null, `${randomName}${fileExtName}`);
  }

  getImages() {
    const low = require('lowdb')
    const FileSync = require('lowdb/adapters/FileSync')
    const adapter = new FileSync('db.json')
    const db = low(adapter)

    return db.get('images')
  }

  setImages(response) {
    const low = require('lowdb')
    const FileSync = require('lowdb/adapters/FileSync')
    const adapter = new FileSync('db.json')
    const db = low(adapter)

    db.get('images')
      .push(response)
      .write()
  }
}
