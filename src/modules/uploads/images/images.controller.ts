import { Controller, UseInterceptors, UploadedFile, Get, Post, Delete } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express'
import { diskStorage } from 'multer'
import { UploadsService } from '../uploads.service'
import * as moment from 'moment'

const editFileName = (req, file, callback) => {
  const fileExtName = file.originalname;
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${randomName}${fileExtName}`);
};

@Controller('/uploads/images')
export class ImagesController {
  constructor(private readonly uploadsService: UploadsService) {}

  @Get('/')
  getUploadFile() {
    return this.uploadsService.getImages()
  }

  @Post('/')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './storage/images',
      filename: editFileName
    })
  }))
  uploadFile(@UploadedFile() file) {

    const response = {
      id: moment().format('YYYYMMDDhhmmSSSS'),
      originalname: file.originalname,
      mimetype: file.mimetype,
      filename: file.filename,
      size: Number(file.size / 1024)
    }

    this.uploadsService.setImages(response)

    return response
  }
}
