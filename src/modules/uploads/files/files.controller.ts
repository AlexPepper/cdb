import { Controller, UseInterceptors, UploadedFile, Post } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express'
import { diskStorage } from 'multer'

const editFileName = (req, file, callback) => {
  const fileExtName = file.originalname;
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${randomName}${fileExtName}`);
};

@Controller('/uploads/files')
export class FilesController {
  @Post('/')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './storage/files',
      filename: editFileName
    })
  }))
  uploadFile(@UploadedFile() file) {
    console.log(file);
  }
}
