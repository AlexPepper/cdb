import { FileUploadMiddlewareMiddleware } from './file-upload-middleware.middleware';

describe('FileUploadMiddlewareMiddleware', () => {
  it('should be defined', () => {
    expect(new FileUploadMiddlewareMiddleware()).toBeDefined();
  });
});
